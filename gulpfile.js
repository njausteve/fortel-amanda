var gulp = require('gulp'),
    sass = require('gulp-sass'),
    notify = require('gulp-notify'),
    autoprefixer = require('gulp-autoprefixer'),
    browserSync = require('browser-sync').create(),
    reload = browserSync.reload;


//variables

var autoprefixerOptions = {
    browsers: ['last 2 versions', '> 5%', 'Firefox ESR']
};

var sassInput = './src/css/sass/*.scss',
    cssOutput = './src/css';




// Static server 
gulp.task('browser-sync', function () {
    browserSync.init({
        server: {
            baseDir: "./src/"
        },
        port: 8800

    });
});

gulp.task('bs-reload', function(){
    browserSync.reload();
});

gulp.task('default', ['browser-sync' ,'watch', 'sass'], function () {


});


//   sass task

gulp.task('sass', function () {
    return gulp
        .src(sassInput)
        .pipe(sass())
        .on('error', notify.onError({
            title: 'SASS Failed',
            message: `Error(s) occurred during compile! :  <%= error.message %>`
        }))
        .pipe(autoprefixer(autoprefixerOptions))
        .pipe(gulp.dest(cssOutput))
        .pipe('bs-reload')
        .pipe(notify({
            message: 'Styles task complete'
        }));
});

// watch task
gulp.task('watch', function(){
   gulp.watch(['./src/*.html','./src/libs/*.*.css', './src/js/*.js', './src/css/*.css'], ['bs-reload']);
   gulp.watch(sassInput, ['sass']);

});